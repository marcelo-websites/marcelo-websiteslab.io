---
title: Início

menu:
  main:
    weight: -1

---


{{% jumbo img="/images/backgrounds/first_image.jpg" imgLabel="TechParty Faccat 2019" %}}

## 23 a 26 Abril, 2019
### Centro de Eventos da Faccat

<a class="btn primary btn-lg" style="margin-top: 20px;" href="https://www.youtube.com/watch?v=UWzdyROzxVI" target="_blank">Assista ao vivo (YouTube) aqui</a>


{{% /jumbo %}}



{{% home-info what="Participantes:640,Dias:4,Sessões:7,Rango Break:4" class="primary" %}}
## O que é a TechParty Faccat?

A TechParty Faccat é um evento totalmente gratuito e aberto à comunidade, visando promover a Tecnologia da Informação (TI) e compartilhamento de conhecimentos. Serão talks durante três noites, com profissionais qualificados e experientes em suas áreas de atuação. Veja na Programação os nossos assuntos e palestrantes. O evento irá fornecer certificado de participação. Quem se inscrever e comparecer, pode contar com ele.
{{% /home-info %}}


{{< youtube-section link="OOrYzR5EviE" title="Veja os melhores momentos do ano passado" class="" >}}

<!-- ... -->



{{% home-speakers %}}
## Quem irá conversar conosco:

<!-- 
{{< button-link label="Submit a presentation"
                url="http://www.conference-hall.io"
                icon="cfp" >}} -->

{{< button-link label="Veja todos"
                url="./speakers"
                icon="right" >}}

{{% /home-speakers %}}





{{% home-subscribe  class="primary" %}}

## É tudo gratuito, das palestras ao Rango Break. Tudo feito com muito carinho e com apoio dos nossos patrocinadores. Inscreva-se aqui: 


{{% /home-subscribe %}}

<!-- ... 

{{% home-tickets %}}
# Tickets

<ul>  
<li>{{< ticket name="Blind Birds"
           starts="2019-04-04"
           ends="2019-11-08"
           price="40 €"
           info="50 first places"
           soldOut="true"
           url="https://www.billetweb.fr/devfest-toulouse-2018" >}}</li>
<li>{{< ticket name="Early Birds"
           starts="2019-04-04"
           ends="2019-11-08"
           price="60 €"
           info="70 first places"
           soldOut="true"
           url="https://www.billetweb.fr/devfest-toulouse-2018" >}}</li>
<li>{{< ticket name="Normal"
           starts="2019-04-04"
           ends="2019-11-08"
           price="80 €"
           info="250 last places"
           soldOut=""
           url="https://www.billetweb.fr/devfest-toulouse-2018" >}}</li>
</ul>

\* Your ticket gives you access to all conferences, coffee breaks, and lunch. Accommodation is NOT included in this price.

{{% /home-tickets %}}

-->

<!-- the map.jpg below actually is a photo of the Convention Build -->

{{% home-location
    image="/images/map.jpg"
    address="Av. Oscar Martins Rangel, 4500. Taquara/RS. Mapa: "
    latitude="-29.638778"
    longitude="-50.786974" %}}

## Local

### Centro de Eventos da Faccat

O Centro de Eventos da Faccat é um local moderno e com várias comodidades,
localizado no Campus da instituição e com uma das mais belas vistas da região do Vale do Paranhana.

Estamos no meio do caminho entre Novo Hamburgo e Gramado, colados em cidades como Igrejinha, Parobé e Três Coroas. Aproveite para conhecer os prédios históricos de Taquara, a fábrica da Heineken de Igrejinha (é possível agendar passeios) ou passear no Centro Budista de Três Coroas.

{{% /home-location %}}

<!-- ... -->

{{% album images="/images/album/tp_2018/02.jpg,/images/album/tp_2018/08.jpg,/images/album/tp_2018/11.jpg,/images/album/tp_2018/42.jpg,/images/album/tp_2018/43.jpg,/images/album/tp_2018/58.jpg,/images/album/tp_2018/61.jpg,/images/album/tp_2018/88.jpg" %}}

### Um pouco do que rolou na TechParty 2018.

<a class="btn primary" target="_blank" rel="noopener" href="https://www.facebook.com/998390266855641/photos/?tab=album&album_id=1996872083674116">
    Veja todas as fotos
    {{% icon "right" %}}
</a>

{{% /album  %}}



{{% partners categories="platinium,gold,silver,bronze,Nosso Apoio para comunidade" %}}
# Nossos Patrocinadores
{{% /partners %}}

