---
title: FAQs
menu:
  main:
    weight: 80
    
draft: true
---

## General

### What is DevFest Toulouse?

The DevFest, or "Developers Festival", is a technical conference for developers.
It is aimed at students, professionals or simply curious technophiles.
Throughout the day, renowned speakers will present a variety of topics: around mobile development, the web, data, connected objects, the cloud, DevOps, etc... as well as good development practices.
Pauses are used to engage in dialogue and deepen the topics presented.

Software development is a very broad subject and our selection committee is committed to providing a varied and interesting program.
This day will be an opportunity to meet speakers of international but also national renown, indeed we also aim to make an important place for local speakers.

DevFest is part of an [international event](https://developers.google.com/events/devfest/) and is organised in partnership with Google.


### Organizing communities

DevFest Toulouse is an event organized by the developer communities of Toulouse, and administratively supported by GDG Toulouse.
You will find the list of communities at the bottom of the main page.

To make all this possible, an over-vitaminated team of volunteers is working behind the scenes.

## Conference

### Where and when will DevFest take place?

Le DevFest Toulouse 2019 a lieu le jeudi 3 octobre au Centre de Congrés Pierre Baudis.

### How to get there?

The congress centre is ideally located in the city centre.

:airplane: BY PLANE :
From Blagnac airport, the easiest way is to take the airport shuttle to get off at Compans-Cafarelli. You can also take the tramway to the Arena station, then line A of the metro to Jean Jaurès, and finally line B to get off at Compans-Cafarelli.

:car: BY CAR:
A parking lot (subject to charge) is located a stone's throw from the conference.

:train: BY TRAIN :
From Matabiau station, simply take the metro line A to Jean Jaurès, then line B to get off at Compans-Cafarelli.

:metro: IN METRO :
Line B, get off at Compans-Cafarelli station.


### How to become a sponsor/partner?

The partnership file is currently being finalized.

## CFP: Call for Paper

### What is the format of the conferences?

Two types of conferences are available:

- The **lightning talks**, lasting **15 minutes**, no questions asked.
- The **conferences**, lasting **40 minutes**, no questions asked in the room.


### How to become a speaker?

The CFP is not yet open.

### Can we submit a talk to several people?

Yes, on the CFP website, if you are not alone during the presentation, fill in the e-mail of the other speakers, they must have previously registered on the CFP with the same address.
For practical reasons, we do not accept topics involving more than three speakers.

### I proposed a conference. Do I have to buy my ticket?

If your subject (conference or lightning talk) has been selected, we offer you your entry to DevFest, so you do not have to buy your ticket in advance.
If you are presenting a multi-party talk, two tickets will be offered per conference, other speakers will have to buy their own seats (in the absence of other accepted conferences.)

In case your subject is not accepted, you will still be able to buy your ticket after the notification, at the early-bird rate.

### Do you cover travel and accommodation costs?

Speakers presenting a full conference (40 minutes) may request reimbursement of their expenses (travel and accommodation) upon presentation of invoices, and up to a maximum of 250€ per conference (and not per speaker).
For lightning talks, travel expenses are not reimbursed.

### What places do you recommend for accommodation?

The Pierre Baudis Congress Centre is located 15 minutes walk from the hyper-centre of Toulouse, or 5 minutes by metro (Compans-Caffarelli stop).
Many hotels and guest rooms are available nearby.

### What is the expected format for slides?

You can present your conference from your own computer, or provide us with the slides in PDF format in advance.
The slides theme is entirely free, and the presentations will then be broadcast on the YouTube channel GDG France, which already hosts the presentations of previous years.


## Ticketing

### What are the conditions for refunding your ticket?

We allow a refund up to 15 days before the event excluding the processing fee of our ticketing partner.

### Is it necessary to bring an identity document or is there a minimum age to enter the event?

Your ticket is enough, there is no minimum age.

### Can I update my registration information?

Yes, you can change your order information on our ticketing partner's website.

### Is it a problem if the name on my ticket or registration does not match the name of the participant?

Yes, please contact us if you have any specific questions.

### I would like an invoice, how do I get it?

Our association is now subject to VAT. It is therefore possible to obtain an invoice from our ticketing partner.
