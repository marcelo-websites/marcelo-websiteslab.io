---
title: Patrocinadores
menu:
  main:
    weight: 4

draft: false
---

{{% hero %}}

Se você deseja ser um patrocinador, entre em contato:

{{< button-link label="techpartyfaccat@gmail.com"
                url="mailto:techpartyfaccat@gmail.com"
                icon="email" >}} 


{{% /hero %}}


<!-- Parteners list -->

{{% partners categories="platinium,gold,silver,bronze,Nosso Apoio para comunidade" %}}

{{% /partners %}}