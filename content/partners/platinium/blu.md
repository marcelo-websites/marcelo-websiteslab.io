---
title: Blu Pagamentos
type: partner
category: platinium
website: 'http://www.useblu.com.br/'
logo: /images/partners/blu.png
socials: []
---

Blu - A melhor solução de pagamentos para o seu negócio. 

Conta digital, pagamento de fornecedores e antecipações de recebimentos. Saiba mais em: [http://www.useblu.com.br/](http://www.useblu.com.br/)

![blu](/images/partners/blu.png)
