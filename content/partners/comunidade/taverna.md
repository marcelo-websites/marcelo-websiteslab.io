---
title: Taverna Pub e RPG
type: partner
category: Nosso Apoio para comunidade
website: 'https://www.duchess-france.org/'
logo: /images/partners/taverna.png
socials: []
---

Jogos e diversão é na Taverna Pub em Taquara: [https://www.facebook.com/tavernapub.rpg/](https://www.facebook.com/tavernapub.rpg/)

![taverna](/images/partners/taverna-banner.png)
