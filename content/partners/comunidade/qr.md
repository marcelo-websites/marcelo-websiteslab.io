---
title: QR para Máquina Café
type: partner
category: Nosso Apoio para comunidade
website: ''
logo: /images/partners/terca.png
socials: []
---

Esse QR permite acionar a máquina de café dos Cursos da Área de TI da Faccat. O equipamento que faz a leitura do QR, confere em uma base de dados na nuvem se o usuário possui créditos e liga o painel da máquina de café, foi todo feito em um Raspberry Pi. Esse projeto foi feito como TCC (Trabalho de Conclusão de Curso) do Bel. em Sistemas de Informação pela Faccat, William Willrich. Maiores informações aqui: https://www2.faccat.br/portal/?q=node/3262. 

A máquina foi patrocinada pela empresa de um aluno do curso, o Rodrigo Baldasso. A empresa é a LoopHOST: https://www.loophost.com.br/

![qr](/images/partners/terca.png)
