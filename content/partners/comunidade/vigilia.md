---
title: Vigília Nerd - Tudo sobre filmes, séries, HQs e música.
type: partner
category: Nosso Apoio para comunidade
website: 'https://vigilianerd.com.br/'
logo: /images/partners/nerd.jpg
socials: 
    
---

Tudo sobre filmes, séries, HQs e música. A VIGÍLIA NÃO PARA! [https://vigilianerd.com.br/](https://vigilianerd.com.br/)

Vigília Nerd é um site voltado para a cultura geek, focada na divulgação de notícias, críticas, podcasts e opinião.


![vigilia](/images/partners/nerd-banner.jpg)
