---
title: Infisc - Inteligência Fiscal
type: partner
category: gold
website: 'https://www.infisc.com.br/'
logo: /images/partners/infisc.jpg
socials: []
---

A Infisc é uma empresa focada na prestação de serviços de consultoria, no desenvolvimento e comercialização de produtos de tecnologia da informação voltados para o setor público com ênfase no ambiente municipal, mais especificamente na área Fazendária. 

Saiba mais sobre a Infisc - Inteligência Fiscal em: [https://www.infisc.com.br/](https://www.infisc.com.br/)

![infisc](/images/partners/infisc.jpg)
