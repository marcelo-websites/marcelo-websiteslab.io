---
title: Julien Renaux
type: core
subtitle: GDG Toulouse
photo: julien_renaux.jpg
socials:
  - link: 'https://twitter.com/julienrenaux'
    name: Twitter
  - link: 'https://github.com/shprink'
    name: Github

---

