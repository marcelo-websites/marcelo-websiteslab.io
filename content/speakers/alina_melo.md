---
id: alina_melo
name: Alina Silva de Melo
company: CWI Software
featured: false
photo: /images/speakers/alina.jpg
photoUrl: /images/speakers/alina.jpg
#socials:
#  - icon: twitter
#    link: 'https://twitter.com/'
#    name: '@'
#  - icon: linkedin
#    link: 'https://www.linkedin.com/'
#    name: LinkedIn
shortBio: >-
  MBA em Gerenciamento de Projetos.
companyLogo: /images/speakers/company/cwi.jpg
companyLogoUrl: /images/speakers/company/cwi.jpg
#country: 'Brasil'

---

Formada em Análise de Sistemas e MBA em Gerenciamento de Projetos. Há mais de 12 anos trabalhando na área de TI, hoje sou uma entusiasta e fomentadora da Qualidade de Software. 

contato: alina.melo@cwi.com.br