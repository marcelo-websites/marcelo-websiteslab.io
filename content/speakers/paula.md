---
id: paula
name: Paula Severo
#company: PJ Serigrafia
featured: false
photo: /images/speakers/paula.jpg
photoUrl: /images/speakers/paula.jpg
socials:
  - icon: linkedin
    link: 'https://www.linkedin.com/in/paula-severo/'
    name: LinkedIn da Paula

shortBio: 'Técnica em Informática'
#companyLogo: /images/speakers/company/cwi.jpg
#companyLogoUrl: /images/speakers/company/cwi.jpg
#country: 'Brasil'

---

Paula tem 18 anos, é arte finalista na empresa PJ Serigrafia, Técnica em Informática pelo Instituto Federal Sul Riograndense Campus Sapucaia do Sul e estudante de Design na Universidade Feevale. Autora do projeto Phoenix.
