---
id: valentini
name: Juliano Valentini
company: Flybyte
featured: false
photo: /images/speakers/valentini.png
photoUrl: /images/speakers/valentini.png
socials:
  - icon: linkedin
    link: 'https://www.linkedin.com/in/juliano-valentini-090a867/'
    name: 'Linkedin do Juliano'

#  - icon: twitter
#    link: 'https://twitter.com/'
#    name: '@'
# icon: linkedin
#    link: 'http://linkedin.com/in/manoela-nascimento-8467a319'
#    name: LinkedIn de Manu Nascimento
shortBio: >-
  Diretor de Operações na Flybyte.
companyLogo: /images/speakers/company/flybyte.jpg
companyLogoUrl: /images/speakers/company/flybyte.jpg
#country: 'Brasil'

---

Bacharel em Ciência da Computação, possui anos de experiência com redes de computadores e operações de provedores internet.
