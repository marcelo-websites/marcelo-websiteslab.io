---
id: manu_nascimento
name: Manu Nascimento
company: CWI Software
featured: false
photo: /images/speakers/manu_nascimento.jpg
photoUrl: /images/speakers/manu_nascimento.jpg
socials:
#  - icon: twitter
#    link: 'https://twitter.com/'
#    name: '@'
  - icon: linkedin
    link: 'http://linkedin.com/in/manoela-nascimento-8467a319'
    name: LinkedIn de Manu Nascimento
shortBio: "Mestre em Design Estratégico."
companyLogo: /images/speakers/company/cwi.jpg
companyLogoUrl: /images/speakers/company/cwi.jpg
#country: 'Brasil'

---

Líder de Design na CWI Software. Indicada a profissional de design do ano 2018. Sólida experiência na condução e execução de processos design orientados ao desenvolvimento de soluções de produtos e serviços, modelos de negócios e estratégias organizacionais. 

contato: manoela.nascimento@cwi.com.br
