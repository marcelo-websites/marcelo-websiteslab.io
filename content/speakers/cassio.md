---
id: cassio
name: Cássio Farias Machado
company: CWI Software
featured: false
photo: /images/speakers/cassio.jpg
photoUrl: /images/speakers/cassio.jpg
socials:
  - icon: github
    link: 'https://github.com/cassiofariasmachado'
    name: 'GitHub do Cássio'

#  - icon: twitter
#    link: 'https://twitter.com/'
#    name: '@'
# icon: linkedin
#    link: 'http://linkedin.com/in/manoela-nascimento-8467a319'
#    name: LinkedIn de Manu Nascimento
shortBio: >-
  Desenvolvedor na área de pesquisa e inovação da CWI Software.
companyLogo: /images/speakers/company/cwi.jpg
companyLogoUrl: /images/speakers/company/cwi.jpg
#country: 'Brasil'

---

Com maior experiência na plataforma .Net, é entusiasta nas áreas de arquitetura de software, programação funcional e data science. Atualmente atua como desenvolvedor na área de pesquisa e inovação da CWI Software, trabalhando em projeto das mais diversas tecnologias, como C#, Python, Java e outras.
