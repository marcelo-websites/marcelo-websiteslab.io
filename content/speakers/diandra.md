---
id: diandra
name: Diandra Rocha
company: CWI Software
featured: false
photo: /images/speakers/diandra.jpg
photoUrl: /images/speakers/diandra.jpg
socials:
  - icon: github
    link: 'https://github.com/DiandraRocha'
    name: 'GitHub da Diandra'

#  - icon: twitter
#    link: 'https://twitter.com/'
#    name: '@'
# icon: linkedin
#    link: 'http://linkedin.com/in/manoela-nascimento-8467a319'
#    name: LinkedIn de Manu Nascimento
shortBio: "Trabalhando com pesquisa e inovação especialmente em projetos envolvendo Inteligência Artificial na CWI Software."
companyLogo: /images/speakers/company/cwi.jpg
companyLogoUrl: /images/speakers/company/cwi.jpg
#country: 'Brasil'

---

Graduanda em Ciência da Computação pela Unisinos, desenvolvedora backend aspirante a DBA, fascinada por dados e tudo que os envolve, especialmente Data Science.  Atualmente trabalhando com pesquisa e inovação especialmente em projetos envolvendo Inteligência Artificial dentro da CWI Software de São Leopoldo.
