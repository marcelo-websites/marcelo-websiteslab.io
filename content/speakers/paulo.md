---
id: paulo
name: Paulo Ricardo Silva
company: Rede Host
featured: false
photo: /images/speakers/paulo.jpg
photoUrl: /images/speakers/paulo.jpg
socials:
  - icon: linkedin
    link: 'https://www.linkedin.com/in/paulodesigner/'
    name: LinkedIn de Paulo Ricardo
shortBio: "Designer gráfico. Atualmente é UX designer na RedeHost. Já atuou em agências on e offline atendendo contas de empresas de pequeno a grande porte, trabalhando em projetos multidisciplinares com entregas que foram desde um flyer até complexos sistemas web."
companyLogo: /images/speakers/company/redehost.svg
companyLogoUrl: /images/speakers/company/redehost.svg

---

Atualmente é UX designer na RedeHost. Já atuou em agências on e offline atendendo contas de empresas de pequeno a grande porte, trabalhando em projetos multidisciplinares com entregas que foram desde um flyer até complexos sistemas web.
