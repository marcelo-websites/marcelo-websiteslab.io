---
id: alexia
name: Aléxia Pereira
company: CWI Software
featured: false
photo: /images/speakers/alexia.jpg
photoUrl: /images/speakers/alexia.jpg
socials:
  - icon: linkedin
    link: 'https://www.linkedin.com/in/al%C3%A9xia-pereira-64a554161/'
    name:  LinkedIn da Aléxia
shortBio: 'Programadora na empresa CWI Software'
companyLogo: /images/speakers/company/cwi.jpg
companyLogoUrl: /images/speakers/company/cwi.jpg
#country: 'Brasil'

---

Aléxia tem 19 anos e trabalha como programadora na empresa CWI Software. É técnica em informática pelo Instituto Federal Sul Riograndense Campus Sapucaia do Sul, e é autora do projeto Phoenix.