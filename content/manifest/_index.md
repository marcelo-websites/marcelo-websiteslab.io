---
title: Manifesto

menu:
  main:
    weight: 9

draft: false
---


{{% hero %}}
## O que é a TechParty Faccat?

A TechParty é um evento que visa promover a tecnologia (Tech) durante um evento informal e de congregação (Party). Os organizadores convidam profissionais experientes da área de TI (Tecnologia da Informação) para apresentações curtas (normalmente de até 40 minutos). Desenvolvimento de software e hardware, linguagens e ferramentas, metodologias, tudo que envolva TI nos interessa. Normalmente são duas apresentações por noite, mas com intervenções rápidas antes e no meio das apresentações realizadas pelos patrocinadores e parceiros que fazem rápidos sorteios de brindes, apresentam suas empresas, entre outras atividades. Ao fim de cada noite todos os participantes são convidados a participarem do RangoBreak, que é uma espécie de coffee break mais, digamos, "reforçado"  :D. Esse RangoBreak é gratuito a todos os participantes e pode ser oferecido graças ao apoio dos nossos incríveis patrocinadores (obrigado!).

##  
O evento é aberto ao público em geral e totalmente gratuito, incluindo o RangoBreak, que é o nosso momento de integração e networking. Todos os participantes ganham certificado emitido pela Faccat (essas horas podem contar como Atividades Complementares). De certa forma, essa semana da TechParty pode ser considerada a Semana Acadêmica dos Cursos da Área de TI da Faccat.

##   
## O surgimento
Tudo iniciou em 2011 pela iniciativa de um grupo de alunos do curso de Sistemas de Informação da Faccat. Era totalmente organizado por esses alunos e ocorria uma vez por semestre em finais de semanas, com palestras e minicursos para leigos até usuários avançados de computação. Contava com apresentações de profissionais de fora da instituição e, também, apresentações feitas pelos próprios alunos e professores. Essas atividades aconteceram ao longo de 2011 e 2012. Por indisponibilidades diversas, em 2013 o evento não aconteceu.

##   
## Vira um evento anual
Em <a class="btn primary" target="_blank" rel="noopener" href="https://techparty.faccat.br/2014/">2014</a>, estudantes da instituição com o objetivo de oferecer conhecimento para o maior número de pessoas, optam por retornar com o evento, mas agora com organização diferenciada.

A TechParty passou a ser um evento anual, com duração de uma semana inteira, com duas talks (apresentações) por noite e coffee break atraente a fim de estimular a interação das pessoas e o networking. Mantendo a proposta de contar com palestrantes da faculdade e convidados externos, mas sempre com assuntos interessantes e atuais, o evento teve um salto no número de participantes com esse novo formato, ultrapassando 700 pessoas (somadas as 5 noites), entre alunos, ex-alunos, pessoas da comunidade, alunos de escolas da região e representantes de muitas empresas da área de atuação da Faccat.

##  
## Rango Break. A estreia.
Em <a class="btn primary" target="_blank" rel="noopener" href="https://techparty.faccat.br/2015/">2015</a> o evento manteve o mesmo ritmo, com ainda mais público e empolgação dos participantes durante a semana da TechParty. Alguma novidade? Sim! Sai o coffee break e um novo evento é lançado (um evento dentro do evento!): o Rango Break.

A ideia agora é que o momento do lanche seja tão ou mais interessante que as próprias apresentações técnicas. O Rango Break é um momento onde, diariamente, após as duas talks de cada noite, é oferecido alguma comidinha ("rango") especial, tais como o cachorro-quente de uma famosa Towner de Taquara (que veio para o Campus da Faccat especialmente para o evento), ou a "galinhada nerd", feita por alunos do curso, sanduíche "buraco quente" e cupcakes, mais um super-dog feito por alunos e, finalmente, no último dia, o grande Churras de Integração dos cursos de TI da Faccat. Esses rangos são fornecidos sem custo aos participantes, graças ao apoio financeiro das empresas patrocinadoras.

##  
## 2016, um grande crescimento 
<a class="btn primary" target="_blank" rel="noopener" href="https://techparty.faccat.br/2016/">2016</a>, seguimos crescendo! Nesse ano, queríamos buscar profissionais de empresas conhecidas nacionalmente e do exterior. Como sempre, foram realizadas talks realmente incríveis e conseguimos profissionais de empresas renomadas, como Red Hat, Rocket.Chat, We Heart It, SAP, Mozilla, Terra, Arezzo, CWI Software.

##  
## 2017, o evento está consolidado na comunidade e entre os apoiadores
Em <a class="btn primary" target="_blank" rel="noopener" href="https://techparty.faccat.br/2017/">2017</a> o evento foi um sucesso como todos os outros anos. Batemos mais um recorde em presenças e contamos com palestrantes de primeiríssmo nível: desenvolvedores, empreendedores, psicólogos (sim! falando sobre uso de games eletrônicos para superação de traumas), gamers, diretores e ninjas em IA. Mais uma vez foi possível contar com o patrocínio de empresas excepcionais que ajudam MUITO o evento acontecer. Muito obrigado Senac, LoopHOST, Rocket.Chat, Umbler, FlyByte, Infisc Inteligência Fiscal, Bela Viagem, Sanvitron, CR Sistemas e Web e Jorge Bischoff.

##  
## 2018: estreiamos no Centro de Eventos da Faccat
<a class="btn primary" target="_blank" rel="noopener" href="https://techparty.faccat.br/2018/">2018</a>, mais uma grande evolução: o evento passa a acontecer no novo Centro de Eventos da Faccat, um prédio bonito, moderno e com muito mais espaço. Isso permitiu aos patrocinadores exporem produtos e montar estandes, deixando o evento mais atrativo para os participantes. Esse foi mais um ano onde o grande número de patrocinadores mostrou a consolidação do evento dentro da comunidade, cujas empresas regionais e de fora confiam e investem para exporem suas marcas e, principalmente, colaboram de maneira fundamental para que o evento ocorra da melhor forma para os participantes.

##  
## Agora: as mulheres são maioria
Para esta edição de 2019, continuamos trabalhando para fazer um evento atrativo e que valha a pena ser visitado. A ótima novidade e que sem dúvida merece ser registrada na história do evento, é que pela primeira vez teremos mulheres entre os palestrantes: cinco (de um total de nove palestrantes), são mulheres. Infelizmente, por um conjunto de acasos que não esperamos que volte a ocorrer, nas edições anteriores nunca uma mulher havia estado em nossos palcos. Mas neste ano as mulheres são maioria.

##  
## Nossa mensagem
Desejamos que o evento seja sempre um momento de encontro entre a comunidade de estudantes, profissionais e empresas da área de Tecnologia da Informação da região do Vale do Paranhana e demais regiões da área de abrangência das Faculdades Integradas de Taquara. Um evento onde as pessoas possam se conhecer, aprender, trocar ideias e, por que não, se divertir.
{{% /hero %}}



