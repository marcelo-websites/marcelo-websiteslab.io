---
id: 4312
title: 'Qualidade de Software: Ágil X Waterfall. Como escolher?'
language: Português
complexity: Básica
tags:
  - Dia 25 (20h05)
#tags:
#  - Big Data / Internet
#presentation: >-
#  https://fit.faccat.br
#videoId: OOrYzR5EviE
speakers:
  - alina_melo
talkType: Palestra

---

Uma discussão confrontando os benefícios e contraindicações de usar e como usar a Metodologia Ágil ou o Modelo Cascata para os processos de Teste de Software. Será que é preciso mesmo escolher?
