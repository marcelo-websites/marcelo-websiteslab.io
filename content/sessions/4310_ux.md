---
id: 4310
title: UX é mágica?
language: Português
complexity: Básica
tags:
  - Dia 23 (19h30)
#tags:
#  - Big Data / Internet
#presentation: >-
#  https://fit.faccat.br
#videoId: OOrYzR5EviE
speakers:
  - paulo
talkType: Palestra

---

A UX (User Experience) se refere às emoções e atitudes quando usamos algum produto, sistema ou serviço. Somos 100% do tempo impactados por essas experiências com produtos e serviços. O principal desafio é criar um ambiente para que esses momentos sejam bem sucedidos.
