---
id: 4528
title: Tecnologia em prol da sociedade - o Projeto Phoenix
language: Português
complexity: Básica
tags:
  - Dia 25 (19h30)
#presentation: >-
#  http://fit.faccat.br
#videoId: OOrYzR5EviE
speakers:
  - alexia
  - paula
talkType: Palestra

---

Com as mais diferentes aplicações da tecnologia nos dias atuais, há muitas causas sociais que podem ser trabalhadas. Iremos ver um exemplo desse tipo de aplicação, Phoenix: Uma Plataforma para o auxílio na comunicação de adolescentes com depressão.


Breve resumo das palestrantes:

- Aléxia, 19 anos. Programadora na empresa CWI Software, técnica em informática pelo Instituto Federal Sul Riograndense Campus Sapucaia do Sul, autora do projeto Phoenix.
- Paula, 18 anos, arte finalista na empresa PJ Serigrafia, técnica em informática pelo Instituto Federal Sul Riograndense Campus Sapucaia do Sul, estudante de Design na Universidade Feevale, autora do projeto Phoenix.