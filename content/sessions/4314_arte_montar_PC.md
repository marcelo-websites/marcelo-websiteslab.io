---
id: 4314
title: A arte de montar PC's Gamer baratos
tags:
  - Dia 25 (20h40)
language: Português
complexity: Básica
#videoId: OOrYzR5EviE
speakers:
  - miguel
talkType: Palestra

---

Você quer jogar os games do momento, mas só vê PC's gamers de R$ 3.000 pra cima? E se eu te contar que montei computadores com o mesmo desempenho por 1.500 reais? Vamos conversar e ver isso ao vivo   :D
