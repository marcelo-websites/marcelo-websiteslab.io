---
id: 4313
title: Deu match, e agora? Como o design se encaixa na TI.
tags:
  - Dia 23 (20h15)
language: Português
complexity: Básica
#videoId: OOrYzR5EviE
speakers:
  - manu_nascimento
talkType: Palestra

---

Em um cenário que a experiência do usuário é dada como fator competitivo, a relevância do Design torna-se ainda mais significativa. Entretanto, ainda se tem muitas dúvidas em relação a atuação do design no processo de desenvolvimento de softwares. 

Iremos entender como o processo de Design se encaixa ao processo das produtoras de software, favorecendo o desenvolvimento de soluções mais assertivas para os negócios e os usuários.
