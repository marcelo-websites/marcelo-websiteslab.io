---
id: 4311
title: Aplicação de um chatbot utilizando o Watson Assistant
language: Português
complexity: Básica
tags:
  - Dia 24 (20h15)
#tags:
#  - Big Data / Internet
#presentation: >-
#  https://fit.faccat.br
#videoId: OOrYzR5EviE
speakers:
  - diandra
  - cassio
talkType: Palestra

---

Nesta talk, dois dos desenvolvedores por trás do chatbot do Programa Crescer CWI trarão a experiência adquirida ao automatizar tarefas utilizando recursos de processamento de linguagem natural com a Plataforma Watson, da IBM, para o desenvolvimento de um assistente inteligente para auxiliar os candidatos no acompanhamento da sua evolução dentro do processo seletivo do programa.
